from rest_framework import serializers

from orders.models import Order, OrderItem
from catalog.serializers import ProductSerializer


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = 'first_name', 'last_name', 'phone_number', 'email', 'address', 'postal_code', 'city'


class OrderItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderItem
        fields = 'order', 'product', 'price', 'quantity'
