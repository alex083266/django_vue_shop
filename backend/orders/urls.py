from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from orders.views import OrderViewSet, OrderItemViewSet

# Создаем router и регистрируем наш ViewSet
router = routers.DefaultRouter()
router.register(r'order', OrderViewSet)
router.register(r'ordering', OrderItemViewSet)
# URLs настраиваются автоматически роутером
urlpatterns = [
    url(r'^', include(router.urls)),
]
app_name = 'orders'
