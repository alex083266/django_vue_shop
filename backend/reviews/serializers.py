from rest_framework import serializers
from reviews.models import Review
from catalog.serializers import ProductSerializer


class ReviewSerializer(serializers.ModelSerializer):
    product = ProductSerializer(many=True, read_only=True)

    class Meta:
        model = Review
        fields = 'id', 'Name', 'email', 'phone', 'rating', 'comment', 'created', 'updated', 'product'
