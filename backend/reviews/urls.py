from django.conf.urls import url
from django.urls import include
from rest_framework import routers
from reviews.views import ReviewViewSet

# Создаем router и регистрируем наш ViewSet
router = routers.DefaultRouter()
router.register(r'reviews', ReviewViewSet)


# URLs настраиваются автоматически роутером
urlpatterns = [
    url(r'^', include(router.urls)),
]
app_name = 'reviews'
