from rest_framework import serializers
from catalog.models import Product, Category


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = 'id', 'name', 'slug'


class ProductSerializer(serializers.ModelSerializer):
    categories = CategorySerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = 'id', 'category', 'name', 'image', 'description', 'price', 'stock', 'available', 'categories',
