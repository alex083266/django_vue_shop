from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from . import views
router = routers.DefaultRouter()
router.register('products', views.ProductViewSet)
router.register('categories', views.CategoryViewSet)
urlpatterns = [
    url(r'^', include(router.urls)),
]
app_name = 'catalog'
